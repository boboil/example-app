import React, { Component } from 'react';
import { Link } from 'react-router';

/**
 *
 */
class HomePage extends Component {
    render () {
        return (
            <div className="intro-header">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="intro-message">
                                <h1> Test app</h1>
                                <h3>by Evhen Odarchenko</h3>
                                <hr className="intro-divider"/>
                                <ul className="list-inline intro-social-buttons">
                                    <li>
                                        <Link  to="users" className="btn btn-success btn-lg">
                                            Users list
                                        </Link>
                                    </li>
                                    <li>
                                        <a href="https://boboil@bitbucket.org/boboil/example-app" className="btn btn-default btn-lg"><i className="fa fa-github fa-fw"></i> <span
                                            className="network-name">Source code</span></a>
                                    </li>
                                    <li>
                                        <a href="https://www.linkedin.com/in/%D0%B5%D0%B2%D0%B3%D0%B5%D0%BD%D0%B8%D0%B9-%D0%BE%D0%B4%D0%B0%D1%80%D1%87%D0%B5%D0%BD%D0%BA%D0%BE-349a62108/" className="btn btn-default btn-lg"><i className="fa fa-linkedin fa-fw"></i> <span className="network-name">Linkedin</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default HomePage;
